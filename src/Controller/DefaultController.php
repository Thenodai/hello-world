<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * TODO: Add the double asterisk in the line above for this route to work
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
        $fileSystemSpeed = $this->fileSystemSpeed();
        $networkSpeed = $this->networkSpeed();
        $phpExecutionTime = $this->phpExecutionTime();

        $content = nl2br(
            "File system I\\O speed: $fileSystemSpeed 
            Network speed: $networkSpeed 
            PHP execution time: $phpExecutionTime"
        );

        $this->savePerformanceTestResultsToFile($content);

        return new Response($content);
    }

    private function fileSystemSpeed()
    {
        $startTime = microtime(true);
        for ($i = 0; $i < 1000; $i++) {
            $file = fopen(__DIR__, "r");
        }
        return microtime(true) - $startTime;
    }

    private function networkSpeed()
    {
        $startTime = microtime(true);
        for ($i = 0; $i < 3; $i++) {
            $file = file_get_contents("http://www.google.com");
        }
        return microtime(true) - $startTime;
    }

    private function phpExecutionTime()
    {
        $startTime = microtime(true);
        for ($i = 0; $i < 10; $i++) {
            $a = array_fill(0, 1000, mt_rand(0, 1000));
            $b = array_fill(0, 1000, mt_rand(0, 1000));
            $c = [];
            foreach ($a as $h) {
                foreach ($b as $j) {
                    $c[] = $h * $j;
                }
            }
        }
        return microtime(true) - $startTime;
    }

    private function savePerformanceTestResultsToFile($content)
    {
        $filesystem = new Filesystem();
        $rootDit = $this->get('kernel')->getProjectDir();
        $filesystem->dumpFile($rootDit . '/results.txt', $content);
    }
}
